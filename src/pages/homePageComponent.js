import React, { Component } from 'react';
import { Helmet } from 'react-helmet'

class Home extends Component {
  exampleMethod(){
    console.log('JS is runing')
  }

  head(){
    return(
      <Helmet>
        <title>My page title</title>
      </Helmet>
    )
  }
  render(){
    return (
      <div>
        {this.head()}
        <h1>
          My home page
        </h1>
        <p>Some content</p>
        <button onClick={()=>this.exampleMethod()}>Console</button>
      </div>
    )
  }
}

export default Home;